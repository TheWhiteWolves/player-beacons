package playerbeacons.api.throttle;

import playerbeacons.api.buff.Buff;

import java.util.List;

/***
 * Any item or block that implements this can reduce the corruption in
 * the beacon or throttle the corruption generated by a buff
 */
public interface IThrottle {

	/***
	 * This method would calculate how much corruption should be throttled that is generated by the Buff
	 * @param buff The Buff that will be throttled
	 * @param beaconLevel The amount of levels on the beacon
	 * @param throttleCount The count of IThrottle detected by the beacon
	 * @return How much the corruption will be throttled by
	 */
	public float getCorruptionThrottle(Buff buff, int beaconLevel, int throttleCount);

	/**
	 * Compared to getCorruptionThrottle, this one will return the amount of corruption to be reduced in the beacon itself.
	 * @param currentCorruption The amount of corruption currently stored in the beacon
	 * @param beaconLevel The current level of corruption in the beacon
	 * @return How much corruption should be reduced.
	 */
	public float getCorruptionReduction(float currentCorruption, int currentCorruptionLevel, int beaconLevel);

	/**
	 * A list of Buff's affected by this throttle. Should return a list of the Buff's names, not the Buff itself!
	 * Eg "dig" instead of DigBuff
	 * @return List of Buff's names that this throttle effects.
	 */
	public List<String> getAffectedBuffs();
}
